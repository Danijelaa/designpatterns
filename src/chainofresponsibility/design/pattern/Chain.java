package chainofresponsibility.design.pattern;

/**
 * @author Danijela
 *An interface with methods for solving problem and method to passing data to other instance to solve the problem.
 */
public interface Chain {
	void setNextChain(Chain nextChain);
	void calculate(Numbers numbers);
}
