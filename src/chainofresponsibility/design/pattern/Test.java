package chainofresponsibility.design.pattern;

/**
 * @author Danijela
 *Class for testing Chain of Responsibility Pattern by passing different values for type of calculation.
 */
public class Test {

	public static void main(String[] args) {
		//Creating instances of classes which implement Chain iterface.
		Chain sum=new AddNumbers();
		Chain sub=new SubtractNumbers();
		Chain mul=new MultipyNumbers();
		Chain div=new DivideNumbers();
		//setting instance of AddNumbers to be the first in chain, then SubtrackNumbers, 
		//then MultiplyNumbers and finally DivideNumbers.
		sum.setNextChain(sub);
		sub.setNextChain(mul);
		mul.setNextChain(div);
		
		//Creating new requests for calculation with same numbers but different type of calculation.
		System.out.println("***********************************");
		Numbers numbers1=new Numbers(5, 2, "+");
		sum.calculate(numbers1);
		System.out.println("***********************************");
		Numbers numbers2=new Numbers(5, 2, "-");
		sum.calculate(numbers2);
		System.out.println("***********************************");
		Numbers numbers3=new Numbers(5, 2, "*");
		sum.calculate(numbers3);
		System.out.println("***********************************");
		Numbers numbers4=new Numbers(5, 2, "%");
		sum.calculate(numbers4);
		System.out.println("***********************************");
		Numbers numbers5=new Numbers(5, 2, "fdfd");
		sum.calculate(numbers5);
		
		
	}

}
