package chainofresponsibility.design.pattern;

/**
 * @author Danijela
 *Concrete class which implements Chain interface.
 */
public class AddNumbers implements Chain{

	private Chain nextChain;
	public void setNextChain(Chain nextChain) {
		this.nextChain=nextChain;
		
	}

	public void calculate(Numbers numbers) {
		if(numbers.getCalculationType().equals("+")){
			System.out.println("Sum is "+(numbers.getNumber1()+numbers.getNumber2()));
		}
		else{
			nextChain.calculate(numbers);
		}
	}

	
}
