package strategy.design.pattern;

/**
 * @author Danijela
 *Strategy interface with method (action) which uses Context class ShoppingCart.
 */
public interface PaymentStrategy {

	void pay(double amount);
}
