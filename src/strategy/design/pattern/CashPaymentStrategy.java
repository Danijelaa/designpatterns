package strategy.design.pattern;

public class CashPaymentStrategy implements PaymentStrategy{

	private double amountGiven;
	
	public CashPaymentStrategy(double amountGiven) {
		super();
		this.amountGiven = amountGiven;
	}

	public void pay(double amount) {
		System.out.println("-------------------------------------");
		System.out.println("Paid in cash.");
		System.out.println("Amount given: "+amountGiven+".");
		System.out.println("Change: "+(amountGiven-amount)+".");
	}

}
