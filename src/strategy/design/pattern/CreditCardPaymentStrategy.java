package strategy.design.pattern;

public class CreditCardPaymentStrategy implements PaymentStrategy{

	private long cardNumber;
	
	public CreditCardPaymentStrategy(long cardNumber) {
		super();
		this.cardNumber = cardNumber;
	}

	public void pay(double amount) {
		System.out.println("-------------------------------------");
		System.out.println("Paid by credit card.");
		System.out.println("Credit card number: "+cardNumber);
	}

}
