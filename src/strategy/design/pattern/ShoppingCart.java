package strategy.design.pattern;

import java.text.SimpleDateFormat;
import java.util.Date;

/**Context lement in Strategy Design Pattern.
 * @author Danijela
 *
 */
public class ShoppingCart {
	private SimpleDateFormat format=new SimpleDateFormat("dd.MM.yyyy. HH:mm:ss");
	private Date date;
	private double amount;
	
	public ShoppingCart(Date date, double amount) {
		super();
		this.date = date;
		this.amount = amount;
	}

	/**This method is used from interface PaymentStrategy.
	 * @param paymentStrategy is an instance of one of the classes which implement PaymentStrategy interface.
	 */
	public void pay(PaymentStrategy paymentStrategy){
		System.out.println("**********************************************************");
		System.out.println("Date: "+format.format(date)+".");
		System.out.println("Total amount to pay: "+amount+".");
		paymentStrategy.pay(amount);
	}
}
