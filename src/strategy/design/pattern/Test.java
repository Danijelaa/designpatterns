package strategy.design.pattern;

import java.util.Date;

/**
 * @author Danijela
 *Class used for testing behavior of method pay() of ShoppingCart instance when different instances of parameter proceeded.
 */
public class Test {

	public static void main(String[] args) {
		ShoppingCart sc1=new ShoppingCart(new Date(), 150000);
		sc1.pay(new CashPaymentStrategy(155000));
		
		
		ShoppingCart sc2=new ShoppingCart(new Date(), 25000);
		sc2.pay(new CreditCardPaymentStrategy(12345678));
	}

}
